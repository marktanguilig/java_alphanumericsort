package sortalphabet;
import java.util.*;

public class SortAlphabet implements Comparator{
    public static void main(String[] args) {
      List <String> arrlist = Arrays.asList("c10","a10","a1","a2","b2","b100","b23","b99");
      Collections.sort(arrlist,new SortAlphabet());
      for(String sort:arrlist){
          System.out.println(sort);
      }
    }
    static char charAt(String str, int i){
        if (i >= str.length()){
            return 0;
        }
        else{
            return str.charAt(i);
        }
    }
    public int compare(Object obj1, Object obj2){
        int ia = 0, ib = 0;
        int nza = 0, nzb = 0;
        char ca, cb;
        int result;

        while (true){
            nza = nzb = 0;
            ca = charAt(obj1.toString(), ia);
            cb = charAt(obj2.toString(), ib);

            while (Character.isSpaceChar(ca) || ca == '0'){
                if (ca == '0'){
                    nza++;
                }
                else{
                    nza = 0;
                }
                ca = charAt(obj1.toString(), ++ia);
            }
            while (Character.isSpaceChar(cb) || cb == '0'){
                if (cb == '0'){
                    nzb++;
                }
                else{
                    nzb = 0;
                }
                cb = charAt(obj2.toString(), ++ib);
            }

            if (Character.isDigit(ca) && Character.isDigit(cb)){
                if ((result = compareRight(obj1.toString().substring(ia), obj2.toString().substring(ib))) != 0){
                    return result;
                }
            }
            if (ca == 0 && cb == 0){
                return nza - nzb;
            }

            if (ca < cb){
                return -1;
            }
            else if (ca > cb){
                return +1;
            }
            ++ia;
            ++ib;
        }
    }
    int compareRight(String a, String b){
        int bias = 0;
        int ia = 0;
        int ib = 0;
        for (;; ia++, ib++){
            char ca = charAt(a, ia);
            char cb = charAt(b, ib);

            if (!Character.isDigit(ca) && !Character.isDigit(cb)){
                return bias;
            }
            else if (!Character.isDigit(ca)){
                return -1;
            }
            else if (!Character.isDigit(cb)){
                return +1;
            }
            else if (ca < cb){
                if (bias == 0){
                    bias = -1;
                }
            }
            else if (ca > cb){
                if (bias == 0)
                    bias = +1;
            }
            else if (ca == 0 && cb == 0){
                return bias;
            }
        }
    }
}
